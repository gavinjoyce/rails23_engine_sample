class Rails23EngineSample::HomeController < ApplicationController
  def index
    render :text => "Hi from Rails23EngineSample::HomeController. Account:#{Account}, I18n.t('txt.admin.views.voice.settings._greetings.voicemail_type'): #{I18n.t('txt.admin.views.voice.settings._greetings.voicemail_type')}"
  end
end
