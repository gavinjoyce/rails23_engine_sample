# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |s|
  s.name        = "rails23_engine_sample"
  s.version     = "0.0.11"
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Gavin Joyce"]
  s.email       = ["gavinjoyce@gmail.com"]
  s.homepage    = "https://bitbucket.org/gavinjoyce/rails23_engine_sample"
  s.summary     = "A sample rails 2.3 engine"
  s.description = "A sample rails 2.3 engine"

  s.required_rubygems_version = ">= 1.3.6"

  s.add_development_dependency("rake")
  s.add_development_dependency("bundler")
  s.add_development_dependency("rails", "2.3.18")

  #s.files        = Dir.glob("app/**/*")
  s.files         = `git ls-files`.split("\n")
  p "FILES: #{s.files}"
  s.test_files   = Dir.glob("test/**/*")
  s.require_paths = ["lib"]
end