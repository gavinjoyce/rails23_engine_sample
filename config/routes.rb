ActionController::Routing::Routes.draw do |map|
  map.namespace(:rails23_engine_sample) do |rails23_engine_sample|
    rails23_engine_sample.root :controller => "home"
  end
end
